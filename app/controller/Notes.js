Ext.define("NotesApp.controller.Notes",{
	extend: "Ext.app.Controller",
	config:{
		refs:{
			//newNoteBtn: "#new-note-btn"
			notesListContainer : "noteslistcontainer"
		},
		control:{
			//newNoteBtn:{
			//	tap:"onNewNote"
			//}
			notesListContainer:{
				newNoteCommand : "onNewNoteCommand",
				editNoteCommand : "onEditNoteCommand"
			}
		}
	},
	
	//onNewNote:function(){
	//	console.log("onNewNote");
	//},
	
	onNewNoteCommand : function(){
		console.log("onNewNoteCommand");
	},
	onEditNoteCommand: function(list,record){
		console.log("onEditNoteCommand");
	},
	
	launch: function(){
		this.callParent();
		Ext.getStore("Notes").load();
		console.log("launch");
	},
	init:function(){
		this.callParent();
		console.log("init");
	}
});