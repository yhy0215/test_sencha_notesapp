Ext.application({
	name : "NotesApp",
	controllers:["Notes"],
	views:["NotesList","NotesListContainer"],
	models:["Note"],
	stores:["Notes"],
	
	launch : function(){
		console.log("App launch");
		var notesListContainer = {
			xtype: "noteslistcontainer"
		};
		Ext.Viewport.add(notesListContainer);
	}
});